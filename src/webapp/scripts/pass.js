(function() {
	$(document).ready(function() {

		$('input[name="confirm_password"]').on('keypress', function(e) {
                	e.preventDefault();
                	var keycode = e.which;
                	var keystroke = String.fromCharCode(e.keyCode).toLowerCase();
                	if(e.ctrlKey && (keystroke == 'c' || keystroke == 'v')){                 		
                		return false;
                	 }else{
                	 	return true;
                	}
                });
		});
})();