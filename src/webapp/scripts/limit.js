(function() {
        var characters = 10;
        $(document).ready(function() {
                $('input[name="phone"]').on('keypress', function(e) {
                	var keycode = e.which;
                	if(keycode>=48 && keycode <=57){
                 		var val = $(this).val();
                 		if(val.length == 10){
                 			return false;
                 		}
                		return true;
                	 }else if(keycode === 08 || keycode === 02){
                	 	return true;
                	}
                	return false
                });
        });
})();