var express = require('express');
var passport = require('passport');
var LocalStrategy = require('passport-local').Strategy;
var app = express();
var router = express.Router();

app.use(passport.initialize());
app.use(passport.session());

// var mongo = require('mongodb');
// var monk = require('monk');
// var db = monk('localhost:27017/horizon');

var mongoose = require('mongoose/');
mongoose.connect('mongodb://localhost/horizon');

var Schema = mongoose.Schema;
var UserDetail = new Schema({
	email : String,
	password : String
}, {
	collection : 'employee'
});
var UserDetails = mongoose.model('employee', UserDetail);



router.post('/login', passport.authenticate('local', {
	successRedirect : '/loginSuccess',
	failureRedirect : '/loginFailure'
}));

router.get('/loginFailure', function(req, res, next) {
	res.send('Failed to authenticate');
});

router.get('/loginSuccess', function(req, res, next) {
	res.send('Successfully authenticated');
});
passport.serializeUser(function(user, done) {
	done(null, user);
});

passport.deserializeUser(function(user, done) {
	done(null, user);
});

passport.use(new LocalStrategy(function(email, password, done) {
	process.nextTick(function() {
		// Auth Check Logic
	});
}));
passport.use(new LocalStrategy(function(email, password, done) {
	process.nextTick(function() {
		UserDetails.findOne({
			'email' : email,
		}, function(err, user) {
			if (err) {
				return done(err);
			}

			if (!email) {
				return done(null, false);
			}

			if (email.password != password) {
				return done(null, false);
			}

			return done(null, email);
		});
	});
}));
module.exports= router;
